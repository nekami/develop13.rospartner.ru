<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Loader; 
Loader::includeModule("highloadblock"); 
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

if (!empty($_POST["ACTION"]) && !empty($_POST["ID_NEWS"]))
{	
	
	$hlblock = HL\HighloadBlockTable::getById(HLBLOCK_NEWS_RATING)->fetch();
	$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
	$entity_data_class = $entity->getDataClass(); 
	
	
	// исходные данные
	$id_news = $_POST["ID_NEWS"];
	$client_ip_address = GetClientIpAddress();    
	$action  = "";
	if ($_POST["ACTION"] == "news_like") $action  = "Y"; 
	

	// проверка ip-адреса
	$rsData = $entity_data_class::getList(
	[
	   "select" => ["ID"],   
	   "filter" => ["UF_IP_ADDRESS" => $client_ip_address, "UF_ID_NEWS" => $id_news]
	]);

	$ip_address = false;
	
	while($arData = $rsData->Fetch()) 
	{
	   if (!empty($arData["ID"])) $ip_address = $arData["ID"];
	}

	if (!$ip_address)
	{
		// добавление   
		$data = [
			"UF_ID_NEWS" => $id_news,
			"UF_LIKE" => $action,    
			"UF_IP_ADDRESS" => $client_ip_address,
			"UF_DATE_TIME" => date("d.m.Y H:i:s")		
		];

		$result = $entity_data_class::add($data);
		echo json_encode(array('success' => true));
	}
	else 
		echo json_encode(array('success' => false));	
}
?>
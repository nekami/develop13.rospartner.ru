<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;	
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;	

$replacer = function ($matches) use ($APPLICATION) 
{	
	ob_start();
	
	Loader::includeModule("highloadblock"); 
	
	$hlblock = HL\HighloadBlockTable::getById(HLBLOCK_NEWS_RATING)->fetch();
	$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
	$entity_data_class = $entity->getDataClass();

	$id_news = $matches[2];	
	
	// подсчет суммарного количества лайков и дизлайков	
	$rsData = $entity_data_class::getList(
	[
		"select" => ["ID", "UF_LIKE"],   
		"filter" => ["UF_ID_NEWS" => $id_news]
	]);

	$count_like = 0;
	$count_dislike = 0;

	while($arData = $rsData->Fetch()) 
	{
		if (!empty($arData["UF_LIKE"])) 
			++$count_like;
		else 
			++$count_dislike;
	}
	
	if ($matches[1] == "LIKES") 
		echo "<span>$count_like</span>"; 
	
	if ($matches[1] == "DISLIKES") 
		echo "<span>$count_dislike</span>"; 
	
	return ob_get_clean();
};
	
echo preg_replace_callback(
	"|SUM(\w{5,8})_(\d+)|".BX_UTF_PCRE_MODIFIER,	
	$replacer,
	$arResult["CACHED_TPL"]
);

	/*$this->__template->SetViewTarget("sum_likes");	
	
		$count_likes = $arResult["DISPLAY_PROPERTIES"]["SUM_LIKES"]["VALUE"];
	
		if (empty($count_likes))			
			$count_likes = $arResult["DISPLAY_PROPERTIES"]["SUM_LIKES"]["DEFAULT_VALUE"];
		
		echo $count_likes;
		
	$this->__template->EndViewTarget();
	
	
	$this->__template->SetViewTarget("sum_dislikes");
	
		$count_dislikes = $arResult["DISPLAY_PROPERTIES"]["SUM_DISLIKES"]["VALUE"];
		
		if (empty($count_dislikes))			
			$count_dislikes = $arResult["DISPLAY_PROPERTIES"]["SUM_DISLIKES"]["DEFAULT_VALUE"];
			
		echo $count_dislikes;		
		
	$this->__template->EndViewTarget();	*/
	
?>
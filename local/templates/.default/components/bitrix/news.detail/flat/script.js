$(function() 
{
	var news_like = BX.getCookie('news_like');	
	var news_dislike = BX.getCookie('news_dislike');
	
	if (news_like) $('#news_like').children("svg").css({"fill": news_like});
	else if (news_dislike) $('#news_dislike').children("svg").css({"fill": news_dislike});	
	
	$(document).on("click",".rating_news__button",function() 
	{
		var me = this;		
		$.ajax({ 
			url: BX.message('TEMPLATE_PATH')+'/ajax.php', 
			data: {"ACTION":  $(me).attr("id"), 
			       "ID_NEWS": $(me).parent().attr("data-id-news")
				  }, 
			method: 'POST',
			type: 'POST', 
			dataType: 'json',
			success: function(data) 
			{
				if (data.success) 
				{
					$(me).children("svg").css({"fill": "#065fd4"});
					$(me).children("span").text(Number($(me).children("span").text()) + 1);					
					BX.setCookie(''+$(me).attr("id")+'', "#065fd4");
				}			
			},			
			error: function () { console.log("Ошибка ajax-запроса"); }
		});
	});
	
}); 
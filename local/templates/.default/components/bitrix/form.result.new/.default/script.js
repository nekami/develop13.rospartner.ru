$(function() 
{
	$('.submit_save').click(function(event) 
	{
		event.preventDefault();
		
		if ($(this).next('input[type="submit"]').prop('disabled') == true)
		{
			if ($('input.fio-input').val() == '')			
				$('input.fio-input').addClass("errortext").next('div.error_text').text("Поле обязательно для заполнения");
				
			if ($('input.mail-input').val() == '')			
				$('input.mail-input').addClass("errortext").next('div.error_text').text("Поле обязательно для заполнения");
			
			if ($('input.phone-input').val() == '')			
				$('input.phone-input').addClass("errortext").next('div.error_text').text("Поле обязательно для заполнения");
		}
	});	
	
	$('input.fio-input').on('blur', function () {
		CheckingClear(this);		
	});		
			
	// валидация e-mail
	$('input.mail-input').on('blur', function () 
	{
		if(CheckingClear(this)) 
		{ 
			if (($(this).val().match(/.+?\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+/g) || []).length !== 1)
				{ValidationError(this);}
			else 
				{ValidationSuccess(this);} 
		}
	});			
			
	// валидация телефона
	$('input.phone-input').on('blur', function () 
	{
		if(CheckingClear(this)) 
		{ 
			if ($(this).val().indexOf("_") != -1) 
				{ValidationError(this);}		
			else 
				{ValidationSuccess(this);}
		}
	});	
});


function ValidationSuccess(field)
{
	$(field).removeClass('errortext').next('div.error_text').text("");
	$('input[type="submit"]').prop('disabled', false).css({'z-index': 2});	
}

function ValidationError(field)
{
	$(field).addClass("errortext").next('div.error_text').text("Поле не соответвует формату");
	$('input[type="submit"]').prop('disabled', true).css({'z-index': 0});
}

function CheckingClear(field)
{
	if ( $(field).val() == '') 
	{
		$(field).addClass("errortext").next('div.error_text').text("Поле обязательно для заполнения");
		$('input[type="submit"]').prop('disabled', true).css({'z-index': 0});
		return false;
    }
	else 
	{
		$(field).removeClass('errortext').next('div.error_text').text("");
		$('input[type="submit"]').prop('disabled', false).css({'z-index': 2});		
		return true;
	}
}